// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {

    apiKey: 'AIzaSyBrywyTGmfQfiOZhELiou0zXLDN_ikgN5k',
    authDomain: 'congresovalorcompartido-7278b.firebaseapp.com',
    databaseURL: 'https://congresovalorcompartido-7278b.firebaseio.com',
    projectId: 'congresovalorcompartido-7278b',
    storageBucket: '',
    messagingSenderId: '924300587055',
    appId: '1:924300587055:web:26c2787a7dcb4f236196da',
    measurementId: 'G-5XHXB285X4'
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
