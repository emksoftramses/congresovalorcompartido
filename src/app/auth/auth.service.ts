import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { User } from './user';
import { AngularFireAuth } from '@angular/fire/auth';
import { MatSnackBar } from '@angular/material';
import { UserService } from '../services/user.service';
import { AppServiceService } from '../services/app-service.service';
import { Usuario } from '../models/usuario';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  loggedIn = new BehaviorSubject<boolean>(false); // {1}
  username: string;
  constructor(
    private userService: UserService,
    private appService: AppServiceService,
    private router: Router,
    private afAuth: AngularFireAuth,
    private snackBar: MatSnackBar
  ) {
    this.afAuth.auth.onAuthStateChanged((user) => {
      if (user) {
        this.loggedIn.next(true);
      } else {
        this.loggedIn.next(false);
      }
    });
  }

  /**
   *
   */
  get isLoggedIn() {
    return this.loggedIn.asObservable(); // {2}
  }

  /**
   *
   * @param user
   */
  login(user: User) {
    this.username = user.userName.toString();
    this.userService.getUserInformation(this.username)
      .subscribe(userInformation => {
        if (userInformation) {
          this.userService.setUser(userInformation);
          this.userService.setUserName(this.username);
          this.redirectProfile(userInformation);
        } else {
          this.snackBar.open('Usuario no registrado, por favor verifique', 'Aceptar', {
            duration: 1500,
          });
        }
      });
  }

  /**
   *
   * @param userType
   */
  public redirectProfile(userInformation: Usuario) {
    this.loggedIn.next(true);
    if (userInformation.tipo_usuario === '1') {
      this.router.navigate(['/admin']);
    } else if (userInformation.tipo_usuario === '2') {
      this.appService.getCongressInformation(userInformation.idCongreso)
        .subscribe(congressInformation => {
          this.appService.setCongress(congressInformation);
          this.router.navigate(['/coor']);
        });

    } else if (userInformation.tipo_usuario === '3') {
      this.appService.getCongressInformation(userInformation.idCongreso)
        .subscribe(congressInformation => {
          this.appService.setCongress(congressInformation);
          this.router.navigate(['/']);
        });
    }
  }

  /**
   *
   */
  logout() {
    this.afAuth.auth.signOut();
    this.loggedIn.next(false);
    this.router.navigate(['/login']);
  }
}

