import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { MatSnackBar, MatDialog } from '@angular/material';
import * as firebase from 'firebase';
import { AdministrarUsuariosService } from './administrar-usuarios.service';
import { map } from 'rxjs/operators';
import { EventSchedule } from '../models/event';
import { Schedule } from '../models/schedule';
import { Observable } from 'rxjs';
import { Congress } from '../models/congress';
import { InvestmentRounds } from '../models/InvestmentRounds';
import { InvestmentOption } from '../models/InvestmentOption';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class AppServiceService {

  private congressInformation: Congress;

  private informacionApllicacionApp: any;
  private rondasInversion: any[];
  private calendarios: any[];
  private appInfo: any;
  private rondaInversion: any;
  public information: any[] = [];
  private informacion: any[] = [];
  private aplicacionInicializada: boolean;
  private idCongreso: number;

  constructor(
    private afs: AngularFirestore,
    private snackBar: MatSnackBar,
    private router: Router,
    private administrarusuarios: AdministrarUsuariosService
  ) { }

  /**
   *
   * @param idOption
   * @param investmentOption
   */
  public qualifyInvestmentOption(idOption: string, investmentOption: any) {
    this.afs.collection('opcionesInversion')
      .doc(idOption)
      .set(investmentOption)
      .then(value => {
        this.snackBar.open('Se creso la ideda correctamente', 'Aceptar', {
          duration: 1500,
        });
      });
  }

  /**
   *
   * @param idOption
   * @param investmentOption
   */
  public createInvestmentOption(idOption: string, investmentOption: InvestmentOption) {
    const option = Object.assign({}, investmentOption);
    this.afs.collection('opcionesInversion')
      .doc(idOption)
      .set(option)
      .then(value => {
        this.router.navigate(['/labs']);
        this.snackBar.open('Se creso la ideda correctamente', 'Aceptar', {
          duration: 1500,
        });
      });
  }

  /**
   * 
   * @param idCongress
   */
  public getInvestmentsOptions(idCongress: number): Observable<any[]> {
    return this.afs
      .collection('opcionesInversion', ref => ref.where('idCongreso', '==', idCongress))
      .valueChanges();
  }

  /**
   * 
   * @param idCongress
   */
  public getInvestmentsOptionsByIdArea(idInvestmentArea: string): Observable<any[]> {
    return this.afs
      .collection('opcionesInversion', ref => ref.where('idAreaInversion', '==', idInvestmentArea))
      .valueChanges();
  }

  /**
   *
   * @param idCongress
   * @param idGroup
   */
  public groupAlreadySentIdea(idCongress: number, idGroup: string) {
    this.getInvestmentsOptions(idCongress).subscribe(investmentOptions => {
      const sendIdeas = investmentOptions.filter(option => option.id_grupo === idGroup);
      console.log(sendIdeas);
    });
    return true;
  }

  /**
   * 
   */
  public getCongress(): Congress {
    return this.congressInformation;
  }

  /**
   *
   * @param congress
   */
  public setCongress(congress: Congress): void {
    this.congressInformation = congress;
  }

  /**
   * 
   * @param idCongress
   */
  public getCongressInformation(idCongress: number) {
    return this.afs
      .collection<Congress>('congresses')
      .doc<Congress>(idCongress.toString())
      .valueChanges();
  }

  /**
   *
   */
  public getCongresses(): Observable<Congress[]> {
    return this.afs.collection<Congress>('congresses').valueChanges();
  }

  /**
   *
   * @param congressInformation
   */
  public createCongress(congressInformation: Congress) {
    const congress = Object.assign({}, congressInformation);
    this.afs
      .collection<Congress>('congresses')
      .doc(congressInformation.idCongress.toString())
      .set(congress)
      .then((value) => {
        this.snackBar.open('Se ha creado el nuevo congreso correctamente', 'Aceptar', {
          duration: 1500,
        });
      });
  }

  /**
   *
   * @param a
   * @param b
   */
  private sortByHour(a, b) {
    if (a.horario > b.horario) {
      return 1;
    }
    if (a.horario < b.horario) {
      return -1;
    }
    return 0;
  }

  /**
   *
   * @param congressId
   */
  public getSchedule(congressId: number): Observable<Schedule> {
    return this.afs.collection('calendars')
      .doc(congressId.toString())
      .snapshotChanges()
      .pipe(map(congressScheduleInfomation => {
        const schedule = new Schedule();
        const scheduleInformation = congressScheduleInfomation.payload.data();
        if (scheduleInformation['events']) {
          schedule.idCongress = scheduleInformation['idCongress'];
          schedule.events = scheduleInformation['events'].map(event => {
            const eventEschedule = new EventSchedule();
            eventEschedule.idEvent = event['idEvent'];
            eventEschedule.schedule = event['schedule'];
            eventEschedule.isLab = event['isLab'];
            eventEschedule.description = event['description'];
            return eventEschedule;
          });
          schedule.events = schedule.events.sort(this.sortByHour);
        }
        return schedule;
      }));
  }

  /**
   *
   * @param idCongress
   */
  getLabs(idCongress: number): Observable<EventSchedule[]> {
    return this.getSchedule(idCongress).pipe(map(schedule => {
      const scheduleLabs = schedule.events.filter(event => event.isLab);
      return scheduleLabs;
    }));
  }

  /**
   *
   * @param schedule
   * @param idCongreso
   */
  public createSchedule(schedule: Array<EventSchedule>, congressId: number) {
    const newSchedule = {
      idCongress: congressId,
      events: schedule
    };

    this.afs
      .collection('calendars')
      .doc(congressId.toString())
      .set(newSchedule)
      .then(() => {
        this.snackBar.open('Se ha creado la ronda de inversion correctamente', 'Aceptar', {
          duration: 1500,
        });
      });
  }

  /**
   *
   * @param schedule
   * @param congressID
   */
  public updateSchedule(schedule: Array<EventSchedule>, congressID: string) {
    const scheduleAsObject = schedule.map(event => Object.assign({}, event));
    this.afs
      .collection('calendars')
      .doc(congressID)
      .update({
        events: firebase.firestore.FieldValue.delete()
      })
      .then(value => {
        this.afs
          .collection('calendars')
          .doc(congressID)
          .update({
            events: scheduleAsObject
          })
          .then(resultUpdate => {
            this.snackBar.open('Se actualizaron correctamente los datos', 'Aceptar', {
              duration: 1500,
            });

          });
      });
  }

  /**
   *
   * @param idCongress
   */
  getInvestmentsRounds(idCongress: number): Observable<InvestmentRounds[]> {
    return this.afs
      .collection<InvestmentRounds>('invesmentRounds', ref => ref.where('congressId', '==', idCongress))
      .valueChanges();
  }



  /**
   * 
   * @param idCongress
   * @param idInvestmentRound
   */
  getInvestmentRound(idCongress: number, idInvestmentRound): Observable<InvestmentRounds> {
    return this.afs.collection<InvestmentRounds>('invesmentRounds')
      .doc<InvestmentRounds>(this.formatIdInvestmentRound(idCongress, idInvestmentRound))
      .snapshotChanges()
      .pipe(map(investmentRoundInformation => {
        const information = investmentRoundInformation.payload.data();
        return information;
      }));
  }

  /**
   *
   * @param idCongress
   * @param idInvestmentRound
   */
  getWinnersInInvestmentRound(idCongress: number, idInvestmentRound): Observable<any[]> {
    return this.getInvestmentRound(idCongress, idInvestmentRound).pipe(map(investmentRound => {
      return investmentRound.winners;
    }));
  }

  /**
   *
   * @param investmentRound
   * @param investmentRoundId
   * @param congressId
   */
  public createInvestmentRounds(investmentRound: InvestmentRounds, investmentRoundId: number, congressId: number): void {
    const investmentRoundAsObject = Object.assign({}, investmentRound);
    this.afs
      .collection('invesmentRounds')
      .doc(this.formatIdInvestmentRound(congressId, investmentRoundId))
      .set(investmentRoundAsObject)
      .then(() => {
        this.snackBar.open('Se ha creado la ronda de inversion correctamente', 'Aceptar', {
          duration: 1500,
        });
      });
  }

  /**
   *
   * @param congressId
   * @param acctualInvestmentRound
   */
  private formatIdInvestmentRound(congressId: number, acctualInvestmentRound: number): string {
    return `${congressId};${(acctualInvestmentRound - 1)}`;
  }

  /// refactor  hacia arriba

  public setCongressId(congressId: number) {
    this.idCongreso = congressId;
  }

  public getCongressId(): number {
    return this.idCongreso;
  }

  getAplicacionInicializada() {
    return this.aplicacionInicializada;
  }



  getCalendario(idCongreso) {

    return this.calendarios.filter(calendario => calendario['idCongreso'] === (idCongreso));
  }

  getLaboratorios(idCongreso) {
    const calendario = this.getCalendario(idCongreso);
    if (calendario.length === 0) {
      return calendario;
    } else {
      return calendario[0]['calendario'].filter(evento => evento['esLaboratorio'] === true);
    }

  }



  getRondasInversion(idCongreso) {
    return this.rondasInversion.filter(rondaInversion => rondaInversion['idCongreso'] === idCongreso);
  }





  getInformacionCongresos() {
    return this.informacion;
  }

  getInformationApp() {
    return this.information;
  }

  getInformacionCongreso(idCongreso: any) {
    return this.informacion[(idCongreso - 1)];
  }



  cambiaRondaInversion(idCongreso) {
    this.afs
      .collection('aplicación')
      .doc(idCongreso.toString())
      .update({
        ronda_inversion_actual: this.getInformacionCongreso(idCongreso)['ronda_inversion_actual'] + 1
      })
      .then(valorGanadores => {
        this.snackBar.open('Se actualizaron correctamente los datos', 'Aceptar', {
          duration: 1500,
        });

      });
  }

  getAppInfo() {
    return this.appInfo;
  }
}
