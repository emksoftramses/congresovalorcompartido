import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { BehaviorSubject, Observable } from 'rxjs';
import { switchMap, map } from 'rxjs/operators';
import { Usuario } from '../models/usuario';
import { MatSnackBar } from '@angular/material';
import { Congress } from '../models/congress';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  userInfo: Usuario;
  userName: string;
  congressInformation: Congress;

  private ideaEnviada: boolean;
  public tasks: Observable<{}[]>;
  private taskDoc: AngularFirestoreDocument<{}>;
  private infoUser$: BehaviorSubject<String>;
  private queryObservable: Observable<{}[]>;
  public email: String;
  public grupo: String;
  public laboratorio: String;
  public puntosIndividuales: Number;
  public puntosTotales: Number;

  constructor(
    public afs: AngularFirestore,
    private snackBar: MatSnackBar,
    private router: Router,
  ) {
  }

  // refactor

  /**
   *
   */
  public enterLaboratory( idLab: number): void {
    this.afs.doc('usuarios/' + this.getUserName() )
      .update({
        'laboratorio': idLab.toString()
      }).then(value => {
        this.router.navigate(['/labs']);
        this.snackBar.open('Inscripcion realizada exitosamente', 'Aceptar', {
          duration: 1500,
        });
      });
  }

  /**
   * 
   */
  public enterGroup( idGroup: string): void {
    this.afs.doc('usuarios/' + this.getUserName() )
      .update({
        'grupo': idGroup
      }).then(value => {
        this.router.navigate(['/labs']);
        this.snackBar.open('Inscripcion realizada exitosamente', 'Aceptar', {
          duration: 1500,
        });
      });
  }

  /**
   * 
   */
  public getUserName(): string {
    return this.userName;
  }

  /**
   * 
   * @param userName
   */
  public setUserName(userName: string): void {
    this.userName = userName;
  }

  /**
   * 
   */
  public getUser(): Usuario {
    return this.userInfo;
  }

  /**
   * 
   */
  public setUser(userInfo: Usuario): void {
    this.userInfo = userInfo;
  }

  /**
   * 
   * @param userNameToConsult
   */
  public getUserInformation(userNameToConsult: string): Observable<Usuario> {
    return this.afs
      .collection<Usuario>('usuarios')
      .doc<Usuario>(userNameToConsult)
      .valueChanges()
      .pipe(map(user => {
        return user;
      }));
  }

  // refactor


  updateInformationUser() {
    this.infoUser$ = new BehaviorSubject<String>('');
    this.queryObservable = this.infoUser$.pipe(
      switchMap((email) =>
        this.afs.collection('usuarios', ref => {
          let query: firebase.firestore.CollectionReference | firebase.firestore.Query = ref;
          query = query.where('correo', '==', email);
          return query;
        }).valueChanges()
      )
    );
  }

  actualizarPuntajeUsuario(nuevoPuntaje: number, puntosDisponibles: number) {
    this.afs
      .collection('usuarios')
      .doc(this.userName)
      .update({
        puntos_individuales: nuevoPuntaje,
        puntos_disponibles: puntosDisponibles
      })
      .then(() => {
        this.snackBar.open('Puntaje actualizado correctamente', 'Aceptar', {
          duration: 1500,
        });
      })
      .catch(err => {
        this.snackBar.open('Hub un error al actualizar el puntaje', 'Aceptar', {
          duration: 1500,
        });
      });
  }

  actualizarInversiones(inversiones: Object[]) {
    this.afs
      .collection('usuarios')
      .doc(this.userName)
      .update({
        inversiones: inversiones
      })
      .then(() => {
        this.snackBar.open('Inversiones actualizadas correctamente', 'Aceptar', {
          duration: 1500,
        });
      })
      .catch(err => {
        this.snackBar.open('Hub un error al actualizar las inversiones', 'Aceptar', {
          duration: 1500,
        });
      });
  }

  setInformationUser(user: Usuario) {
    this.userInfo = user;
    if (this.userInfo.laboratorio !== '' && this.userInfo.grupo !== '') {
      this.afs.collection(
        'opcionesInversion',
        ref => ref.where('id_grupo', '==', this.userInfo.grupo)
      )
        .valueChanges()
        .subscribe(data => {
          if (data.length === 0) {
            this.ideaEnviada = false;
          } else {
            this.ideaEnviada = true;
          }
        });
    } else {
      this.ideaEnviada = false;
    }
  }

  getIdCongreso() {
    return this.getInformationUser().idCongreso;
  }

  getInformationUser() {
    return this.userInfo;
  }

  getIdeaEnviada() {
    return this.ideaEnviada;
  }

  setIdeaEnviada(estado: boolean) {
    this.ideaEnviada = estado;
  }

  public userIsAdmin() {
    return (this.getInformationUser().tipo_usuario === '1');
  }

}
