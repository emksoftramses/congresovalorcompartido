export class InvestmentRounds {
    criteria: Array<any>;
    winners: Array<any>;
    name: string;
    numberWinners: number;
    topics: Array<any>;
    congressId: number;

    public getCriteriaAsString(): string {
        return this.criteria.join( ', ' );
    }
}
