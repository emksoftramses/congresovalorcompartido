export class Congress {
    idCongress: number;
    nameCongress: string;
    currentInvestmentRound: number;
    totalNumberRounds: number;
}
