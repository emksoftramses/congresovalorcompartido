import { Usuario } from './usuario';

export interface UsuarioId extends Usuario {
    id: string;
}
