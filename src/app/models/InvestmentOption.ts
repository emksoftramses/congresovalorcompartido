export class InvestmentOption {
    nombreInversion: string;
    descripcion: string;
    impact: string;
    opportunity: string;
    user: string;
    idAreaInversion: string;
    estado: string;
    calificacion: Object;
    inversores: Array<any>;
    id_grupo: string;
    idCongreso: number;
}
