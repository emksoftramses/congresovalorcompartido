import { EventSchedule } from "./event";

export class Schedule {
    idCongress: number;
    events: Array<EventSchedule>;
}
