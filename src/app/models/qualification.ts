export class Qualification {
    ratingByUserName: string;
    ratingByEmail: string;
    values: Array<number>;
}
