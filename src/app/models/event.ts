export class EventSchedule {
    idEvent: number;
    schedule: string;
    description: string;
    isLab: boolean;
}
