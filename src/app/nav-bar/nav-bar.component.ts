import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserService } from '../services/user.service';
import { AuthService } from '../auth/auth.service';
import { AppServiceService } from '../services/app-service.service';
import { Usuario } from '../models/usuario';
import { Congress } from '../models/congress';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css'],
})
export class NavBarComponent {

  user: Usuario;
  congress: Congress;
  nameCongress: string;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(
    private breakpointObserver: BreakpointObserver,
    private userService: UserService,
    private appService: AppServiceService,
    private authService: AuthService
  ) {
    this.user = this.userService.getUser();

    if (this.user) {
      this.appService.getCongressInformation(this.user.idCongreso).subscribe(congress => {
        this.nameCongress = congress.nameCongress;
      });
    }
  }

  cerrarSesion() {
    this.authService.logout();
  }

}
