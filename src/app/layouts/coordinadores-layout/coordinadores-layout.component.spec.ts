import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoordinadoresLayoutComponent } from './coordinadores-layout.component';

describe('CoordinadoresLayoutComponent', () => {
  let component: CoordinadoresLayoutComponent;
  let fixture: ComponentFixture<CoordinadoresLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoordinadoresLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoordinadoresLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
