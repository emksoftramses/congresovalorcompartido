import { Component, OnInit, Input } from '@angular/core';
import { AppServiceService } from 'src/app/services/app-service.service';
import { DialogAgregarEventoComponent } from '../dialog-agregar-evento/dialog-agregar-evento.component';
import { MatDialog } from '@angular/material';
import { UserService } from 'src/app/services/user.service';
import { EventSchedule } from 'src/app/models/event';
import { Observable } from 'rxjs';
import { Schedule } from 'src/app/models/schedule';
import { Congress } from 'src/app/models/congress';

@Component({
  selector: 'app-administrar-agenda',
  templateUrl: './administrar-agenda.component.html',
  styleUrls: ['./administrar-agenda.component.css']
})
export class AdministrarAgendaComponent implements OnInit {

  @Input()
  congressInformation: Congress;

  scheduleInformation: Observable<Schedule>;
  events: Array<EventSchedule>;
  numberOfEvents: number;
  typeUserId: number;
  canAddEvent: boolean;
  constructor(
    private userService: UserService,
    private appService: AppServiceService,
    private dialog: MatDialog,
  ) { }

  ngOnInit() {
    if ( !this.congressInformation ) {
      this.congressInformation = this.appService.getCongress();
    }
    this.scheduleInformation = this.appService.getSchedule(this.congressInformation.idCongress);
    this.scheduleInformation.subscribe(schedule => {
      if (schedule.events) {
        this.numberOfEvents = schedule.events.length;
        this.events = schedule.events;
      } else {
        this.events = new Array<EventSchedule>();
      }
    });
    this.typeUserId = Number.parseInt(this.userService.getInformationUser().tipo_usuario);
    this.canAddEvent = this.userService.userIsAdmin();
  }

  public abrirDialogoNuevoEvento(): void {
    const dialogRef = this.dialog.open(DialogAgregarEventoComponent, {
      width: '250px',
      data: {
        horarioDesde: undefined,
        horarioHasta: undefined,
        descripcion: '',
        esLaboratorio: false
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.horarioDesde && result.horarioHasta && result.descripcion) {
        const desde = this.formatDate(result.horarioDesde.hour, result.horarioDesde.minute);
        const hasta = this.formatDate(result.horarioHasta.hour, result.horarioHasta.minute);
        const newEventInformation = new EventSchedule();
        newEventInformation.idEvent = this.events.length;
        newEventInformation.schedule = `${desde} - ${hasta}`;
        newEventInformation.description = result.descripcion;
        newEventInformation.isLab = result.esLaboratorio;
        this.events.push(newEventInformation);
        this.appService.updateSchedule(
          this.events,
          this.congressInformation.idCongress.toString()
        );
        this.numberOfEvents = this.events.length;
      }
    });
  }


  private formatDate(hour: number, minute: number) {
    let hourAsString = hour.toString();
    let minuteAsString = minute.toString();
    if ( hour < 10) {
      hourAsString = `0${hourAsString}`;
    }

    if ( minute < 10) {
      minuteAsString = `0${minuteAsString}`;
    }

    return `${hourAsString}:${minuteAsString}`;
  }
}
