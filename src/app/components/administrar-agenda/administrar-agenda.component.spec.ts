import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministrarAgendaComponent } from './administrar-agenda.component';

describe('AdministrarAgendaComponent', () => {
  let component: AdministrarAgendaComponent;
  let fixture: ComponentFixture<AdministrarAgendaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministrarAgendaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministrarAgendaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
