import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OptionsInvestmentsComponent } from './options-investments.component';

describe('OptionsInvestmentsComponent', () => {
  let component: OptionsInvestmentsComponent;
  let fixture: ComponentFixture<OptionsInvestmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OptionsInvestmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OptionsInvestmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
