import { Component, OnInit, Input } from '@angular/core';
import { AppServiceService } from 'src/app/services/app-service.service';
import { UserService } from 'src/app/services/user.service';
import { Usuario } from 'src/app/models/usuario';
import { Congress } from 'src/app/models/congress';
import { MatDialog } from '@angular/material';
import { InvestmentRounds } from 'src/app/models/InvestmentRounds';
import { CalificarIdeaModalComponent } from '../calificar-idea-modal/calificar-idea-modal.component';

@Component({
  selector: 'app-options-investments',
  templateUrl: './options-investments.component.html',
  styleUrls: ['./options-investments.component.css']
})
export class OptionsInvestmentsComponent implements OnInit {

  @Input()
  idInvestmentArea: string;

  @Input()
  idInvestmentRound: number;

  congress: Congress;
  investmentsOptions: Array<any>;
  investmentRound: InvestmentRounds;
  user: Usuario;
  isUserCoor: boolean;
  isFinalUser: boolean;
  displayedColumns: string[];
  constructor(
    private appService: AppServiceService,
    private userService: UserService,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.congress = this.appService.getCongress();

    this.appService.getInvestmentRound( this.congress.idCongress, this.idInvestmentRound )
        .subscribe( investmentRoundInfo => {
          this.investmentRound = investmentRoundInfo;
        });

    this.appService.getInvestmentsOptionsByIdArea( this.idInvestmentArea )
        .subscribe( investments => {
          this.investmentsOptions = investments;
        });

    this.userService.getUserInformation(this.userService.getUserName()).subscribe(userInfo => {
      this.user = userInfo;
      if (this.user.tipo_usuario === '3') {
        this.isFinalUser = true;
        this.isUserCoor = false;
        this.displayedColumns = ['nombreInversion', 'descripcion', 'impact', 'opportunity', 'id_grupo'];
      } else {
        this.isFinalUser = false;
        this.isUserCoor = true;
        this.displayedColumns = ['nombreInversion', 'descripcion', 'impact', 'opportunity', 'id_grupo', 'actions'];
      }
    });
  }

  public addRating( investmentsOption: any, resultRating: any ): void {
    let ratings = [];
    const roundRatings = `ronda_${this.congress.currentInvestmentRound}`;
    if ( investmentsOption.hasOwnProperty( roundRatings ) ) {
      ratings = investmentsOption[roundRatings];
    } else {
      investmentsOption[roundRatings] = [];
    }
    const qualification = {
      ratingByUserName: this.userService.getUserName(),
      ratingByEmail: this.user.correo,
      values: resultRating.respuestasCriterios
    };
    ratings.push( qualification );
    investmentsOption[roundRatings] = ratings;
    const idCongress = this.congress.idCongress;
    const lab = this.user.laboratorio;
    const group = this.user.grupo;
    const idInvestmentOption = `${ idCongress }-1;${lab}-${group}`;
    this.appService.qualifyInvestmentOption( idInvestmentOption, investmentsOption );
  }

  public openDialog(investmentsOption: any): void {
    const respuestasCriterios = this.investmentRound.criteria;
    const dialogRef = this.dialog.open(CalificarIdeaModalComponent, {
      width: '250px',
      data: {
        criterios: respuestasCriterios,
        ronda_inversion_actual: this.congress.currentInvestmentRound,
        respuestasCriterios: []
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if ( result ) {
        this.addRating(investmentsOption, result);
      }
    });
  }

}
