import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Congress } from 'src/app/models/congress';


@Component({
  selector: 'app-crear-congreso',
  templateUrl: './crear-congreso.component.html',
  styleUrls: ['./crear-congreso.component.css']
})

export class CrearCongresoComponent {
  constructor(
    public dialogRef: MatDialogRef<CrearCongresoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
