import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearCongresoComponent } from './crear-congreso.component';

describe('CrearCongresoComponent', () => {
  let component: CrearCongresoComponent;
  let fixture: ComponentFixture<CrearCongresoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearCongresoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearCongresoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
