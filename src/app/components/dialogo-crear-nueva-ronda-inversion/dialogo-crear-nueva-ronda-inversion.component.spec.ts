import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogoCrearNuevaRondaInversionComponent } from './dialogo-crear-nueva-ronda-inversion.component';

describe('DialogoCrearNuevaRondaInversionComponent', () => {
  let component: DialogoCrearNuevaRondaInversionComponent;
  let fixture: ComponentFixture<DialogoCrearNuevaRondaInversionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogoCrearNuevaRondaInversionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogoCrearNuevaRondaInversionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
