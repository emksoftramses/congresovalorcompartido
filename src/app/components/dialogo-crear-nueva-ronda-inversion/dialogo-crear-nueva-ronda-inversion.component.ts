import { Component, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-dialogo-crear-nueva-ronda-inversion',
  templateUrl: './dialogo-crear-nueva-ronda-inversion.component.html',
  styleUrls: ['./dialogo-crear-nueva-ronda-inversion.component.css']
})

export class DialogoCrearNuevaRondaInversionComponent {

  constructor(
    public dialogRef: MatDialogRef<DialogoCrearNuevaRondaInversionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Object) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}
