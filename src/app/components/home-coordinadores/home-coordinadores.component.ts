import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { MatDialog } from '@angular/material';
import { from } from 'rxjs';
import { AppServiceService } from 'src/app/services/app-service.service';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-home-coordinadores',
  templateUrl: './home-coordinadores.component.html',
  styleUrls: ['./home-coordinadores.component.css']
})
export class HomeCoordinadoresComponent implements OnInit {

  animal: string;
  name: string;
  private email: string;
  private preguntas: Object[];
  constructor(
    private userService: UserService,
    public dialog: MatDialog,
    private appService: AppServiceService,
    private afs: AngularFirestore
  ) {
    this.email = userService.getInformationUser().correo;
    /*
    this.afs
      .collection('laboratorios')
      .doc(userService.getInformationUser().laboratorio)
      .valueChanges()
      .subscribe( infoLaboratorio => {
        this.preguntas = infoLaboratorio['preguntas'];
        this.preguntas = this.preguntas.reverse();
        console.log( this.preguntas );
      });
      */
  }

  ngOnInit() {
  }
}
