import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalificarIdeaModalComponent } from './calificar-idea-modal.component';

describe('CalificarIdeaModalComponent', () => {
  let component: CalificarIdeaModalComponent;
  let fixture: ComponentFixture<CalificarIdeaModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalificarIdeaModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalificarIdeaModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
