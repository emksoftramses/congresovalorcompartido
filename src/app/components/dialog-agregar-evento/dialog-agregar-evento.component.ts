import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Time } from 'src/app/models/time';

@Component({
  selector: 'app-dialog-agregar-evento',
  templateUrl: './dialog-agregar-evento.component.html',
  styleUrls: ['./dialog-agregar-evento.component.css']
})

export class DialogAgregarEventoComponent {

  since: Time;
  until: Time;
  meridian: boolean;

  constructor(
    public dialogRef: MatDialogRef<DialogAgregarEventoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.meridian = true;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  send(): any {
    this.data['horarioDesde'] = this.since;
    this.data['horarioHasta'] = this.until;
    return this.data;
  }

  toggleMeridian() {
    this.meridian = !this.meridian;
  }
}
