import { Component, OnInit, ViewChild } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { MatSnackBar, MatTable, MatTableDataSource, MatPaginator } from '@angular/material';
import { AdministrarUsuariosService } from 'src/app/services/administrar-usuarios.service';
import { Observable } from 'rxjs';
import { UsuarioId } from 'src/app/models/usuario-id';
import { AppServiceService } from 'src/app/services/app-service.service';

@Component({
  selector: 'app-crear-usuario',
  templateUrl: './crear-usuario.component.html',
  styleUrls: ['./crear-usuario.component.css']
})
export class CrearUsuarioComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;

  private textoAlmacenamientoMasivoUsuarioParticipante: string;
  private textoAlmacenamientoMasivoUsuarioCoordinador: string;
  private nombreCompleto: String;
  private identificacion: String;
  private correo: String;
  private nombreCompletoCoor: String;
  private identificacionCoor: String;
  private correoCoor: String;
  private usuariosRegistrados: any;
  private columnasTablaUsuario: string[] = [
    'identificacion',
    'correo',
    'grupo',
    'laboratorio',
    'nombre',
    'tipoUsuario'
  ];
  private usuariosDataSource: MatTableDataSource<UsuarioId>;
  private usuarios: Observable<UsuarioId[]>;

  passCodificadas = '';
  constructor(
    private afs: AngularFirestore,
    private authFirebase: AngularFireAuth,
    private snackBar: MatSnackBar,
    private administrarUsuariosService: AdministrarUsuariosService,
    private informacionUsuarios: AdministrarUsuariosService,
    private appService: AppServiceService
  ) { }

  ngOnInit() {
    this.usuarios = this.informacionUsuarios.getUsuarios();
    this.informacionUsuarios
        .getUsuarios()
        .subscribe(value => {
          this.usuariosDataSource = new MatTableDataSource( value );
          this.usuariosDataSource.paginator = this.paginator;
          console.log( value );
        });
  }

  applyFilter(filterValue: string) {
    this.usuariosDataSource.filter = filterValue.trim().toLowerCase();
  }

  crearUsuario() {
    this.administrarUsuariosService.crearNuevoUsuario(false, this.appService.getInformacionCongresos(), this.appService);
  }
}
