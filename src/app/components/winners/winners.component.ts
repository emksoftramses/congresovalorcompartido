import { Component, OnInit, Input } from '@angular/core';
import { Congress } from 'src/app/models/congress';
import { Observable } from 'rxjs';
import { InvestmentRounds } from 'src/app/models/InvestmentRounds';
import { AppServiceService } from 'src/app/services/app-service.service';

@Component({
  selector: 'app-winners',
  templateUrl: './winners.component.html',
  styleUrls: ['./winners.component.css']
})
export class WinnersComponent implements OnInit {

  @Input()
  congressInformation: Congress;

  winnersInInvestmentRound: Observable<any[]>;
  numberOfWinners: number;

  constructor(
    private appService: AppServiceService,
  ) { }

  ngOnInit() {
    this.numberOfWinners = 0;
    if (this.congressInformation) {
      this.winnersInInvestmentRound = this.appService.getWinnersInInvestmentRound(
        this.congressInformation.idCongress,
        this.congressInformation.currentInvestmentRound
      );
      this.winnersInInvestmentRound.subscribe( winners => this.numberOfWinners = winners.length );
      /*
      this.investmentsRounds = this.appService.getInvestmentRound(
        this.congressInformation.idCongress,
        this.congressInformation.currentInvestmentRound
      ).subscribe(investmentRound => {
        console.log(investmentRound);
      });
      */
    }
  }

}
