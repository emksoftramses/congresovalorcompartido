import { Component, OnInit, Inject } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import { AppServiceService } from 'src/app/services/app-service.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { CalificarIdeaModalComponent } from '../calificar-idea-modal/calificar-idea-modal.component';
import { Congress } from 'src/app/models/congress';
import { Usuario } from 'src/app/models/usuario';
import { InvestmentRounds } from 'src/app/models/InvestmentRounds';
import { EventSchedule } from 'src/app/models/event';

@Component({
  selector: 'app-investments',
  templateUrl: './investments.component.html',
  styleUrls: ['./investments.component.css']
})

export class InvestmentsComponent implements OnInit {

  congress: Congress;
  congressId: number;
  user: Usuario;
  rondasInversion: Array<InvestmentRounds>;
  labs: Array<EventSchedule>;
  panelOpenState = false;

  constructor(
    private userService: UserService,
    private appService: AppServiceService,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    this.congress = this.appService.getCongress();
    this.congressId = this.congress.idCongress;
    this.userService.getUserInformation(this.userService.getUserName()).subscribe(userInfo => {
      this.user = userInfo;
    });

    this.appService.getInvestmentsRounds(this.congressId).subscribe(rounds => {
      this.rondasInversion = rounds;
    });

    this.appService.getLabs(this.congressId).subscribe(labs => {
      this.labs = labs;
    });
  }

  /**
   *
   * @param idLab
   * @param idInvestmentRound
   */
  public buildIdInvestmentArea( idLab: number, idInvestmentRound: number ): string {
    const idCongress = this.congress.idCongress;
    return `${idCongress}-${idInvestmentRound};${idLab}`;
  }
}
