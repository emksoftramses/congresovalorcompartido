import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultsInversionsComponent } from './results-inversions.component';

describe('ResultsInversionsComponent', () => {
  let component: ResultsInversionsComponent;
  let fixture: ComponentFixture<ResultsInversionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultsInversionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultsInversionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
