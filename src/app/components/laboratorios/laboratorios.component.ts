import { Component, OnInit, Inject } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { MatSnackBar } from '@angular/material';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { AppServiceService } from 'src/app/services/app-service.service';
import { Congress } from 'src/app/models/congress';
import { Observable } from 'rxjs';
import { EventSchedule } from 'src/app/models/event';
import { Usuario } from 'src/app/models/usuario';
import { RegistrationGroupComponent } from '../registration-group/registration-group.component';

@Component({
  selector: 'app-laboratorios',
  templateUrl: './laboratorios.component.html',
  styleUrls: ['./laboratorios.component.css']
})
export class LaboratoriosComponent implements OnInit {

  congressInformation: Congress;
  laboratorios: EventSchedule[];
  displayedColumnsLabs: string[] = ['id', 'description', 'action'];
  userInformation: Usuario;
  isSent: boolean;

  constructor(
    private appService: AppServiceService,
    private userService: UserService,
    public dialog: MatDialog,
    private router: Router
  ) {
    this.isSent = false;
    this.congressInformation = this.appService.getCongress();
    this.appService.getLabs(this.congressInformation.idCongress)
      .subscribe(labs => {
        this.laboratorios = labs;
      });

    this.userService.getUserInformation(this.userService.getUserName())
      .subscribe(userInfo => {
        this.userInformation = userInfo;
        if (this.userInformation.grupo) {
          this.ideaSent(this.userInformation.grupo);
        }
      });
  }

  ngOnInit() {
  }

  /**
   * 
   * @param idLab 
   */
  public enterLaboratory(idLab: number): void {
    this.userService.enterLaboratory(idLab);
  }

  /**
   * 
   * @param idGroup 
   */
  public ideaSent(idGroup: string): void {
    this.appService.getInvestmentsOptions(this.congressInformation.idCongress)
        .subscribe( investmentOptions => {
          const sendIdeas = investmentOptions.filter( option => option.id_grupo === idGroup );
          this.isSent = ( sendIdeas.length > 0 );
        });
  }

  /**
   * 
   */
  public openDialog(): void {
    const dialogRef = this.dialog.open(RegistrationGroupComponent, {
      width: '250px',
      data: { group: '' }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.userService.enterGroup(result);
    });
  }

  /**
   * 
   */
  public sendIdea(): void {
    this.router.navigate(['../design_thinking']);
  }

  /**
   * 
   */
  public showLabOption(): boolean {
    return !this.userInformation.laboratorio;
  }

  /**
   *
   * @param idEvent
   */
  public showgroupOption( idEvent: number ): boolean {
    return this.userInformation.laboratorio && !this.userInformation.grupo && this.userInformation.laboratorio === idEvent.toString();
  }

  /**
   *
   * @param idEvent
   */
  public showSendIdeaOption( idEvent: number ): boolean {
    return  this.userInformation.laboratorio &&
            this.userInformation.grupo &&
            this.userInformation.laboratorio === idEvent.toString() &&
            !this.isSent;
  }

  public showMessageSentIdea( idEvent: number ): boolean {
    return this.userInformation.laboratorio === idEvent.toString() && this.isSent;
  }
}

