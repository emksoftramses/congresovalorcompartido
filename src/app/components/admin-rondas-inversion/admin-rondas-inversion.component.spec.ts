import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminRondasInversionComponent } from './admin-rondas-inversion.component';

describe('AdminRondasInversionComponent', () => {
  let component: AdminRondasInversionComponent;
  let fixture: ComponentFixture<AdminRondasInversionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminRondasInversionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminRondasInversionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
