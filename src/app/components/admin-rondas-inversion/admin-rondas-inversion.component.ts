import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { AppServiceService } from 'src/app/services/app-service.service';
import { MatDialog, MatSnackBar } from '@angular/material';
import { NuevoCriterioInversionComponent } from '../nuevo-criterio-inversion/nuevo-criterio-inversion.component';
import { Congress } from 'src/app/models/congress';
import { Observable } from 'rxjs';
import { InvestmentRounds } from 'src/app/models/InvestmentRounds';
import { DialogoCrearNuevaRondaInversionComponent } from '../dialogo-crear-nueva-ronda-inversion/dialogo-crear-nueva-ronda-inversion.component';

@Component({
  selector: 'app-admin-rondas-inversion',
  templateUrl: './admin-rondas-inversion.component.html',
  styleUrls: ['./admin-rondas-inversion.component.css']
})
export class AdminRondasInversionComponent implements OnInit {

  @Input()
  congressInformation: Congress;
  investmentsRounds: Observable<InvestmentRounds[]>;
  idCongress: number;
  numberInvestmentRounds: number;

  private informacionLista = false;
  private appInfo: any;
  private infoRondaInversion: any;
  private nombreNuevoCriterio: String;
  private rondaInversionEvaluar: string;
  constructor(
    private appService: AppServiceService,
    private route: ActivatedRoute,
    private afs: AngularFirestore,
    private dialog: MatDialog,
    private snackBar: MatSnackBar
  ) {
    this.nombreNuevoCriterio = '';
  }

  ngOnInit() {
    if (this.congressInformation) {
      this.idCongress = this.congressInformation.idCongress;
      this.investmentsRounds = this.appService.getInvestmentsRounds(this.idCongress);
      this.investmentsRounds.subscribe( investmentsRounds => {
        this.numberInvestmentRounds = investmentsRounds.length;
      });
    }
  }

  public abrirDialogoNuevaRondaInversion(congressId: number, idInvestment: number = 0): void {
    const nombreNuevaRondaInversion = '';
    const numeroGanadores = 1;
    const dialogRef = this.dialog.open(DialogoCrearNuevaRondaInversionComponent, {
      width: '350px',
      data: {
        nombreRondaInversion: nombreNuevaRondaInversion,
        numeroGanadores: numeroGanadores
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.createInvestmentRound(result, congressId, idInvestment);
    });
  }

  private createInvestmentRound(newInvestmentRoundInformation: any, congressId: number, idInvestment: number = 0): void {
    const newInvestmentRound = new InvestmentRounds();
    newInvestmentRound.criteria = [];
    newInvestmentRound.winners = [];
    newInvestmentRound.name = newInvestmentRoundInformation.nombreRondaInversion;
    newInvestmentRound.numberWinners = newInvestmentRoundInformation.numeroGanadores;
    newInvestmentRound.topics = [];
    newInvestmentRound.congressId = congressId;
    this.appService.createInvestmentRounds(newInvestmentRound, idInvestment, congressId);
  }

  /*
  openDialog(): void {
    const dialogRef = this.dialog.open(NuevoCriterioInversionComponent, {
      width: '250px',
      data: {
        nombreCriterio: this.nombreNuevoCriterio
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined && result.nombreCriterio.trim() !== '') {
        this.infoRondaInversion['criterios'].push(result.nombreCriterio);
        this.afs
          .collection('rondas_inversion')
          .doc(this.rondaInversionEvaluar)
          .update({
            criterios: this.infoRondaInversion['criterios']
          })
          .then(value => {
            this.snackBar.open('Se agrego el nuevo criterio exitosamente', 'Aceptar', {
              duration: 1500
            });
          })
          .catch(err => {
            console.log(err);
          });
      }
    });
  }
  */
}
