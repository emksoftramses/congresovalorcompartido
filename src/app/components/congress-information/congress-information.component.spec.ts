import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CongressInformationComponent } from './congress-information.component';

describe('CongressInformationComponent', () => {
  let component: CongressInformationComponent;
  let fixture: ComponentFixture<CongressInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CongressInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CongressInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
