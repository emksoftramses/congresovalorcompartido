import { Component, OnInit, Input } from '@angular/core';
import { Congress } from 'src/app/models/congress';
import { AppServiceService } from 'src/app/services/app-service.service';
import { Observable } from 'rxjs';
import { InvestmentRounds } from 'src/app/models/InvestmentRounds';

@Component({
  selector: 'app-congress-information',
  templateUrl: './congress-information.component.html',
  styleUrls: ['./congress-information.component.css']
})
export class CongressInformationComponent implements OnInit {

  @Input()
  congressInformation: Congress;

  constructor(
    private appService: AppServiceService
  ) {
    this.congressInformation = new Congress;
   }

  ngOnInit() {
    const idCongress = this.congressInformation.idCongress;
  }

}
