import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/firestore';
import { MatSnackBar } from '@angular/material';
import { UserService } from 'src/app/services/user.service';
import { AppServiceService } from 'src/app/services/app-service.service';
import { ActivatedRoute } from '@angular/router';
import { Congress } from 'src/app/models/congress';
import { Usuario } from 'src/app/models/usuario';
import { InvestmentOption } from 'src/app/models/InvestmentOption';

@Component({
  selector: 'app-design-thinking',
  templateUrl: './design-thinking.component.html',
  styleUrls: ['./design-thinking.component.css']
})
export class DesignThinkingComponent implements OnInit {

  congressInformation: Congress;
  form: FormGroup;
  formSubmitAttempt: boolean;
  userInformation: Usuario;

  constructor(
    private appService: AppServiceService,
    private fb: FormBuilder,
    private afs: AngularFirestore,
    private snackBar: MatSnackBar,
    private userService: UserService,
    private appInformation: AppServiceService,
    private route: ActivatedRoute
  ) {
    /*
    appInformation.getAppInfo()
    .subscribe( value => {
      this.appInfo = value;
    });
    */
  }

  ngOnInit() {
    this.congressInformation = this.appService.getCongress();
    this.userService.getUserInformation(this.userService.getUserName())
      .subscribe(userInfo => {
        this.userInformation = userInfo;
      });

    this.form = this.fb.group({
      nameIdea: ['', Validators.required],
      opportunity: ['', Validators.required],
      description: ['', Validators.required],
      impact: ['', Validators.required],
    });
  }

  /**
   *
   * @param field
   */
  public isFieldInvalid(field: string) {
    return (
      (!this.form.get(field).valid && this.form.get(field).touched) ||
      (this.form.get(field).untouched && this.formSubmitAttempt)
    );
  }


  /**
   * 
   */
  onSubmit() {
    
    if (this.form.valid) {
      const idCongress = this.congressInformation.idCongress; 
      const currentInvestmentRound = this.congressInformation.currentInvestmentRound;
      const lab = this.userInformation.laboratorio;
      const group = this.userInformation.grupo;
      const idInvestmentOption = `${ idCongress }-1;${lab}-${group}`;
      const newInvestmentOption = new InvestmentOption();
      newInvestmentOption.nombreInversion = this.form.value['nameIdea'];
      newInvestmentOption.descripcion = this.form.value['description'];
      newInvestmentOption.impact = this.form.value['impact'];
      newInvestmentOption.opportunity = this.form.value['opportunity'];
      newInvestmentOption.user = this.userService.getUserName();
      newInvestmentOption.idAreaInversion = `${ idCongress }-${currentInvestmentRound};${lab}`;
      newInvestmentOption.estado = `idea`;
      newInvestmentOption.calificacion = {};
      newInvestmentOption.inversores = [],
      newInvestmentOption.id_grupo = group;
      newInvestmentOption.idCongreso = idCongress;
      this.appService.createInvestmentOption( idInvestmentOption, newInvestmentOption );
      this.formSubmitAttempt = true;
    }
  }

}
