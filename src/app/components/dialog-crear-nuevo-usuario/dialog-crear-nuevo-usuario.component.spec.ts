import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogCrearNuevoUsuarioComponent } from './dialog-crear-nuevo-usuario.component';

describe('DialogCrearNuevoUsuarioComponent', () => {
  let component: DialogCrearNuevoUsuarioComponent;
  let fixture: ComponentFixture<DialogCrearNuevoUsuarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogCrearNuevoUsuarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogCrearNuevoUsuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
