import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-registration-group',
  templateUrl: './registration-group.component.html',
  styleUrls: ['./registration-group.component.css']
})
export class RegistrationGroupComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<RegistrationGroupComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
  }

}
